# Aide et ressources de Fullprof pour Synchrotron SOLEIL

[![](https://www.ill.eu/sites/fullprof/img/logoprueba.gif)](https://www.ill.eu/sites/fullprof/)

## Résumé
- Suite logicielle. Sert à l'affinement de poudres. Analyse complète
- Privé

## Sources
- Code source: https://www.ill.eu/sites/fullprof/php/downloads.html
- Documentation officielle: https://www.ill.eu/sites/fullprof/php/programs.html

## Navigation rapide
| Wiki SOLEIL | Tutoriaux |
| ------ | ------ |
| [Tutoriel d'installation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/fullprof-documentation/-/wikis/Setup-guide) | [Tutoriaux officiels](https://www.ill.eu/sites/fullprof/php/tutorials.html) |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/fullprof/wikis/home) | |

## Installation
- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien), Soucis droits administrateurs

## Format de données
- en entrée: diffractogrammes 1D, formats propriétaires, vaste variété de formats
- en sortie: coordonnées (.cif)
- sur un disque dur, sur la Ruche, clef usb
